<?php

namespace App\DataFixtures;

use App\Entity\Book;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory;

class BookFixtures extends Fixture
{
    private static $images = [
        "images/13_fairytale.jpg",
        "images/451F.jpg",
        "images/euphoria.jpg",
        "images/milkman.jpg",
        "images/mint_tale.jpg",
        "images/perfume.jpg",
        "images/maid.jpg"
    ];

    protected $faker;

    public function load(ObjectManager $manager)
    {
        $faker = Factory::create();

        for($i = 0; $i<15; $i++) {
            $book = new Book();
            $book->setTitle($faker->realText(20));
            $book->setAuthor($faker->name);
            $book->setImageUrl($faker->randomElement(self::$images));
            $book->setDescription($faker->realText(50));
            $manager->persist($book);
        }
        $manager->flush();
    }
}
