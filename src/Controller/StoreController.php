<?php
namespace App\Controller;

use App\Entity\Book;
use App\Form\CommentFormType;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Response as ResponseA;
use Symfony\Component\Routing\Annotation\Route;


class StoreController extends AbstractController
{
    /**
     * @Route("/")
     * @param EntityManagerInterface $entityManager
     * @param PaginatorInterface $paginator
     * @param Request $request
     * @return ResponseA
     */
    public function catalogPage(EntityManagerInterface $entityManager, PaginatorInterface $paginator, Request $request)
    {
        $repository = $entityManager->getRepository(Book::class);

        $pagination = $paginator->paginate(
            $repository->getQuery(), /* query NOT result */
            $request->query->getInt('page', 1)/*page number*/,
            10/*limit per page*/
        );
        return $this->render('book/book_store.html.twig', [
            'articleContent' => $this->articleContent(),
            'books' => $pagination
        ]);
    }

    /**
     * @Route("/{slug}")
     * @param $slug
     * @param EntityManagerInterface $entityManager
     * @param Request $request
     * @return Response
     */
    public function bookPage($slug, EntityManagerInterface $entityManager, Request $request)
    {
        $repository = $entityManager->getRepository(Book::class);
        $book = $repository->find($slug);

        $form = $this->createForm(CommentFormType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $comment= $form->getData();
            $comment->setBook($book);
            $entityManager->persist($comment);
            $entityManager->flush();
        }



        return $this->render('book/book.html.twig', [
            //'title' => ucwords(str_replace('-', ' ', $slug)),
            'book' => $book,
            'articleContent' =>  $this->articleContent(),
            'commentForm' => $form->createView(),
            'comments' => $book->getComments()
    ]);
    }

    private function articleContent()
    {
        $articleContent = <<<EOF

Can't find a cute and interesting book? Tired of the same suggestions
all bookstores, and you want something unusual? Then to you in our shop!
EOF;
        return $articleContent;
    }

}